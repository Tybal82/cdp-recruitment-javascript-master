
// Import utils
const appendChildrenLengthOnParentName = require('./appendChildrenLengthOnParentName');
const filterDataByAnimalPattern = require('./filterDataByAnimalPattern');

// CLI
const init = (data, argument) => {

    let processedData = null;
    let currentArgument = argument ? argument : process.argv[2];

    // Assign the correct handler based on argument
    currentArgument.split("=")[0] === '--count' 
    ? processedData = appendChildrenLengthOnParentName(data)
    : processedData = filterDataByAnimalPattern(data, currentArgument)

    // console.log(JSON.stringify(processedData, null, 2));
    return processedData;
}

module.exports = init;

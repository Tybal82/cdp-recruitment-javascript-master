// Function to append children length on parent name
const appendChildrenLengthOnParentName = (data) =>
    data.map(({name, people}) => ({
      name : buildName(name, people.length), // Current name + children length
      people : people.map(({name, animals}) => ({
          name : buildName(name, animals.length), // Current name + children length
          animals,
        }
      ))
    }
  )
)

// build new name
const buildName = (name, childrenLength) => `${name} [${childrenLength}]`;

module.exports = appendChildrenLengthOnParentName 
const filterDataByAnimalPattern = (data, argument) => {

  let pattern = argument.split('=');

  // Check if argument has an option or not 
  pattern.length > 1 ? pattern = pattern[1] : null

  return data.map(({name, people}) => {
    const currentCountry = {
      name,
      people: people.map(({name, animals}) => {
        const currentPeople = {
          name,
          animals: animals.filter(({name}) => name.includes(pattern))
        };
        // Only people with animals
        return currentPeople.animals.length > 0 ? currentPeople : null 
      }).filter(person => person !== null) // Remove null values
    };
    // Only countries with people 
    return currentCountry.people.length > 0 ? currentCountry : null 
  }).filter(country => country !== null) // Remove null values
}
   
module.exports = filterDataByAnimalPattern
const { data } = require('../data');
const appendChildrenLengthOnParentName = require('../utils/appendChildrenLengthOnParentName')
const countChildrenHandlerExpectedValue = require('./mocks/countChildrenHandlerExpectedValue')

test('should append children length on parent name', () => {
    expect(appendChildrenLengthOnParentName(data)).toStrictEqual(countChildrenHandlerExpectedValue);
});


const filterHandlerExpectedValue = [
    {
      "name": "Dillauti",
      "people": [
        {
          "name": "Winifred Graham",
          "animals": [
            {
              "name": "Anoa"
            }
          ]
        },
        {
          "name": "Philip Murray",
          "animals": [
            {
              "name": "Xenops"
            },
            {
              "name": "Dinosaur"
            }
          ]
        }
      ]
    },
    {
      "name": "Uzuzozne",
      "people": [
        {
          "name": "Georgia Hooper",
          "animals": [
            {
              "name": "Rhinoceros"
            }
          ]
        }
      ]
    }
  ]

  module.exports = filterHandlerExpectedValue
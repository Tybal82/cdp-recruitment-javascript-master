const { data } = require('../data');
const init = require('../utils/init');
const filterHandlerExpectedValue = require('./mocks/filterHandlerExpectedValue');
const countChildrenHandlerExpectedValue = require('./mocks/countChildrenHandlerExpectedValue');

test('should apply the right handler from CLI argument', () => {
    expect(init(data, '--filter=no')).toStrictEqual(filterHandlerExpectedValue);
});

test('should append children length on parent name', () => {
    expect(init(data, '--count')).toStrictEqual(countChildrenHandlerExpectedValue);
});

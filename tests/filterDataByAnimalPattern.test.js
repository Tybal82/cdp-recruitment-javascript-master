const { data } = require('../data');
const filterDataByAnimalPattern = require('../utils/filterDataByAnimalPattern');
const filterHandlerExpectedValue = require('./mocks/filterHandlerExpectedValue');

test('should filter data by animal pattern from CLI', () => {
    expect(filterDataByAnimalPattern(data, '--filter=no')).toStrictEqual(filterHandlerExpectedValue);
});

 

